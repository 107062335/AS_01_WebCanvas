var canvas, ctx, pre_x, pre_y, cur_x, cur_y, flag_d, flag_t, flag_c, flag_s;
var mode, color = 'black', thick,  thick_e;
var f_size,  f_style;
var shape_x, shape_y;
var redo_arr = [], undo_arr = [], arr_len = 12, can_redo;
var c1 = true;

function init() {
    canvas = document.getElementById('myCanvas');
    ctx = canvas.getContext("2d");
    var can_pos = canvas.getBoundingClientRect();
    console.log(can_pos.top, can_pos.bottom, can_pos.left, can_pos.right);
    console.log("offset", canvas.offsetLeft, canvas.offsetTop);

    cur_x = -canvas.offsetLeft;
    cur_y = -canvas.offsetTop;
    pre_x = pre_y  = 0;
    flag_t = false;
    flag_c = false;
    falg_s = false;
    can_redo = false;
    f_size = 14;
    f_style = "Times New Roman";
    ctx.fillStyle = 'rgba(255, 255, 255, 1)';
    ctx.fill();

    canvas.addEventListener("mousemove", (m)=> {
        mouse_event('move', m);
     }, false);
    canvas.addEventListener("mousedown", (m)=> {
        mouse_event('down', m);
    }, false);
    canvas.addEventListener("mouseup", (m)=> {
        mouse_event('up', m);
    }, false);
    canvas.addEventListener("mouseout", (m)=> {
        mouse_event('out', m);
    }, false);
    canvas.addEventListener("mouseover", (m)=> {
        mouse_event('over', m);
    }, false);

    //save_cur('undo');

}

function draw() {
   // console.log("drawing");
    ctx.beginPath();
    ctx.strokeStyle = color;
    /*if(thick > 5) {
        ctx.arc(cur_x, cur_y + 20, thick, 0, Math.PI*2);
        ctx.fill();
    }
    else {*/
        ctx.moveTo(pre_x, pre_y + 20);
        ctx.lineTo(cur_x, cur_y + 20);
        ctx.lineWidth = thick;
        ctx.lineCap = "round";
        ctx.stroke();
    //}
    ctx.closePath();
}


function draw_line() {
    console.log("draw line");
    var shape_img = new Image();
    shape_img.src = undo_arr[undo_arr.length - 1];
    shape_img.onload = () =>{
    clear_c();
    ctx.globalCompositeOperation="source-over";
    ctx.drawImage(shape_img, 0, 0);
    //console.log(shape_img);
    ctx.beginPath();
    ctx.moveTo(shape_x, shape_y);
    ctx.lineTo(cur_x, cur_y);
    ctx.lineWidth = 8;
    ctx.stroke();
    ctx.closePath();
    };
    delete shape_img;
    //console.log('erase ', pre_x, pre_y);
    //console.log('stroke ', cur_x, cur_y);
}


function draw_cir() {
    var shape_img = new Image();
    shape_img.src = undo_arr[undo_arr.length - 1];
    shape_img.onload = () =>{
    clear_c();
    ctx.globalCompositeOperation="source-over";
    ctx.drawImage(shape_img, 0, 0);
    ctx.beginPath();
    ctx.lineWidth = 8;
    var cir_x = (shape_x + cur_x) / 2;
    var cir_y = (shape_y + cur_y) / 2;
    var r = Math.abs(shape_x - cur_x);
    var s = Math.abs((shape_y - cur_y)/r);
    // /ctx.scale(s, 1);
    ctx.arc(cir_x, cir_y, r, 0, 2*Math.PI);
    ctx.stroke();
    ctx.closePath();
    };
    delete shape_img;
}

function draw_rect() {
    var shape_img = new Image();
    shape_img.src = undo_arr[undo_arr.length - 1];
    shape_img.onload = () =>{
    clear_c();
    ctx.globalCompositeOperation="source-over";
    ctx.drawImage(shape_img, 0, 0);
    //console.log(shape_img);
    ctx.beginPath();
    ctx.lineWidth = 8;
    if(cur_x < shape_x && cur_y < shape_y) {
        ctx.rect(cur_x, cur_y, shape_x - cur_x, shape_y - cur_y);
    }
    else if(cur_x < shape_x) {
        ctx.rect(cur_x, shape_y, shape_x - cur_x, cur_y - shape_y);
    }
    else if(cur_y < shape_y) {
        ctx.rect(shape_x, cur_y, cur_x - shape_x, shape_y - cur_y);
    }
    else {
        ctx.rect(shape_x, shape_y, cur_x - shape_x, cur_y - shape_y);
    }
    ctx.stroke();
    ctx.closePath();
    };
    delete shape_img;
}

function draw_tri() {
    var shape_img = new Image();
    shape_img.src = undo_arr[undo_arr.length - 1];
    shape_img.onload = () =>{
    clear_c();
    ctx.globalCompositeOperation="source-over";
    ctx.drawImage(shape_img, 0, 0);
    //console.log(shape_img);
    ctx.beginPath();
    ctx.lineWidth = 8;
    var top = (shape_x + cur_x) / 2;
    ctx.moveTo(cur_x, cur_y);
    ctx.lineTo(top, shape_y);
    ctx.lineTo(shape_x, cur_y);
    ctx.closePath();
    ctx.stroke();
    };
    delete shape_img;
}


function eee() {
    console.log("erasing");
    ctx.globalCompositeOperation="destination-out";
    ctx.beginPath();
    //Number(thick_e);
    /*var x = cur_x + Number(thick_e);
    var y = cur_y + 0.5 * Number(thick_e);
    ctx.arc(x, y, thick_e, 0, Math.PI*2, false);
    ctx.fill();
    ctx.closePath();*/
    ctx.moveTo(pre_x + Number(thick_e), pre_y);
    ctx.lineTo(cur_x + Number(thick_e), cur_y);
    ctx.lineWidth = thick_e* 1.5;
    ctx.lineCap = "round";
    ctx.stroke();
}

function mouse_event(event, m) {
    if (event == 'down') {
        if(!flag_s) {
            save_cur('undo');
        }
        pre_x = cur_x;
        pre_y = cur_y;
        cur_x = m.clientX - canvas.offsetLeft;
        cur_y = m.clientY - canvas.offsetTop + 10;
        //console.log("mouse pos", m.clientX, m.clientY);
        if(mode == 'draw') {
            can_redo = false;
            redo_arr.splice(0, redo_arr.length);
            flag_d = true;
            ctx.globalCompositeOperation="source-over";
            ctx.beginPath();
            ctx.fillStyle = color;
            var b_size = document.getElementById('brush_r');
            thick = b_size.value;
            //if(thick > 5) {
                ctx.arc(cur_x, cur_y + 20, thick/2, 0, Math.PI*2);
                ctx.fill();
            /*}
            else {
                ctx.fillRect(cur_x, cur_y + 20, thick, thick);
                ctx.closePath();
            }*/
        }
        else if(mode == 'text') {
            if(flag_t) {
                var tb = document.getElementById('text_box');
                //var fc = document.getElementById('fc');
                drawText(tb.value, parseInt(tb.style.left, 10), parseInt(tb.style.top, 10));
                document.body.removeChild(tb);
                flag_t = false;
            }
            else {
                console.log("add input box");
                var in_text = document.createElement('input');
                //var fc = document.getElementById('fc');
                in_text.id = 'text_box';
                in_text.type = 'text';
                in_text.style.position = 'fixed';
                in_text.style.left = (cur_x + canvas.offsetLeft) + 'px';
                in_text.style.top = (cur_y + canvas.offsetTop - 25) + 'px';
                in_text.onkeydown = handleEnter;
                document.body.appendChild(in_text);
                flag_t = true;
            }
        }
        else if(mode == 'erase') {
            flag_d = true;
            can_redo = false;
            redo_arr.splice(0, redo_arr.length);
            ctx.globalCompositeOperation="destination-out";
            var e_size = document.getElementById('erase_r');
            thick_e = e_size.value;
            chg_mode('erase');
            eee();
        }
        else if(mode == 'line') {
            if(flag_s) {
                draw_line();
            }
            else {
                can_redo = false;
                redo_arr.splice(0, redo_arr.length);
                shape_x = cur_x;
                shape_y = cur_y;
                ctx.lineWidth = 8;
            }
            flag_s = !flag_s;
        }
        else if(mode == 'cir') {
            if(flag_s) {
                draw_cir();
            }
            else {
                can_redo = false;
                redo_arr.splice(0, redo_arr.length);
                shape_x = cur_x;
                shape_y = cur_y;
                ctx.lineWidth = 8;
            }
            flag_s = !flag_s;
        }
        else if(mode == 'rect') {
            if(flag_s) {
                draw_rect();
            }
            else {
                can_redo = false;
                redo_arr.splice(0, redo_arr.length);
                shape_x = cur_x;
                shape_y = cur_y;
                ctx.lineWidth = 8;
            }
            flag_s = !flag_s;
        }
        else if(mode == 'tri') {
            if(flag_s) {
                draw_tri();
            }
            else {
                can_redo = false;
                redo_arr.splice(0, redo_arr.length);
                shape_x = cur_x;
                shape_y = cur_y;
                ctx.lineWidth = 8;
            }
            flag_s = !flag_s;
        }
    }
    if (event == 'up' || event == "out") {
        flag_d = false;
        if(mode == 'text' && flag_t) {
            document.getElementById('text_box').focus();
        }
    }
    if(event == 'over') {
        var e_size = document.getElementById('erase_r');
        thick_e = e_size.value;
        chg_mode(mode);
    }
    if (event == 'move') {
        pre_x = cur_x;
        pre_y = cur_y;
        cur_x = m.clientX - canvas.offsetLeft;
        cur_y = m.clientY - canvas.offsetTop + 10;
        if(mode == 'draw') {
            if(flag_d)
                draw();
        }
        else if(mode == 'erase') {
            if(flag_d)
                eee();
        }
        else if(mode == 'line') {
            if(flag_s)
                draw_line();
        }
        else if(mode == 'cir') {
            if(flag_s)
                draw_cir();
        }
        else if(mode == 'rect') {
            if(flag_s)    
                draw_rect();
        }
        else if(mode == 'tri') {
            if(flag_s)    
                draw_tri();
        }
    }
}

function drawText(txt, x, y) {
    can_redo = false;
    redo_arr.splice(0, redo_arr.length);
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.font = f_size + 'px ' + f_style;
    ctx.fillStyle = color;
    ctx.globalCompositeOperation="source-over";
    ctx.fillText(txt, x - canvas.offsetLeft - 4, y - canvas.offsetTop + 15);
}

function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        //var fc = document.getElementById('fc');
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        flag_t = false;
    }
}

// change mode
function chg_mode(n_mode) {

    mode = n_mode;
    console.log('change mode to', n_mode);
    if(mode == 'draw') {
        //document.getElementById('b_d').style.backgrond = "gold";
        canvas.style.cursor = 'url(./img/brush_c.png), auto';
    }
    else if(mode ==  'text') {
        //document.getElementById('b_x').style = "backgrond-color: gold";
        canvas.style.cursor = 'text';
    }
    else if(mode == 'erase') {
        //document.getElementById('b_e').style = "backgrond-color: gold";
        //console.log(thick_e);
        if(thick_e > 17) {
            canvas.style.cursor = 'url(./img/eee_c4.png), auto';
        } 
        else if(thick_e > 12) {
            canvas.style.cursor = 'url(./img/eee_c3.png), auto';
        }
        else if(thick_e > 7) {
            canvas.style.cursor = 'url(./img/eee_c2.png), auto';
        }
        else {
            canvas.style.cursor = 'url(./img/eee_c1.png), auto';
        }
    }
    /*else if(mode == 'line') {
        document.getElementById('b_l').style = "backgrond-color: gold";
    }
    else if(mode == 'rect') {
        document.getElementById('b_r').style = "backgrond-color: gold";
    }
    else if(mode == 'tri') {
        document.getElementById('b_t').style = "backgrond-color: gold";
    }
    else if(mode == 'cri') {
        document.getElementById('b_c').style = "backgrond-color: gold";
    }*/
    else {
        canvas.style.cursor = 'default';
    }
    console.log(canvas.style.cursor);
}


// reset canvas
function clear_c() {
    ctx.globalCompositeOperation="destination-out";
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if(flag_t) {
        var tb = document.getElementById('text_box');
        //var fc = document.getElementById('fc');
        drawText(tb.value, parseInt(tb.style.left, 10), parseInt(tb.style.top, 10));
        document.body.removeChild(tb);
        flag_t = false;
    }
    //console.log('clear canvas');
}


// change color for brush text and shape
function chg_col(col) {
    console.log("change to color ", col);
    color = col;
    if(c1 == true) {
        var col_idx = document.getElementById('col1');
        col_idx.style.fill = col;
    }
    else {
        var col_idx = document.getElementById('col2');
        col_idx.style.fill = col;
    }

}

// change color from big icon
function chg_colb(tt) {
    console.log("change to color ", tt.style.fill);
    color = tt.style.fill;
    if(tt.id == 'col1') {
        if(!c1) {
            c1 = true;
            tt.style.stroke = "darkorange";
            document.getElementById('col2').style.stroke = "white";
        }
    }
    else {
        if(c1) {
            c1 = false;
            tt.style.stroke = "darkorange";
            document.getElementById('col1').style.stroke = "white";
        }
    }
}

// select color from color picker
function col_sel() {
    var sel = document.getElementById('c_col');
    sel.click();
}

// change font size
function chg_tsize(ts) {
    f_size = ts.value;
    console.log("change font size to ", ts.value);
}

// change font style
function chg_tstyle(ts) {
    f_style = ts.value;
    console.log("change font style to ", ts.value);
}

function undo() {
    can_redo = true;
    if(undo_arr.length > 0) {
        console.log("undo");
        save_cur('redo');
        var last_img = new Image();
        last_img.src = undo_arr.pop();
        last_img.onload = ()=>{ 
        clear_c();
        ctx.globalCompositeOperation="source-over";
        ctx.drawImage(last_img, 0, 0);};
    }
    delete last_img;
}

function save_cur(mode) {
    //console.log("save current state");
    var image = document.getElementById('myCanvas').toDataURL();
    if(mode == 'undo') {
        undo_arr.push(image);
        console.log("save current state");
        if(undo_arr.length > arr_len) { 
            undo_arr.shift();
            console.log("too many prev state");
        }
    }
    else if(mode == 'redo') {
        redo_arr.push(image);
    }
}

function  redo() {
    if(redo_arr.length > 0 && can_redo) {
        console.log("redo");
        save_cur('undo');
        var last_img = new Image();
        last_img.src = redo_arr.pop();
        last_img.onload = ()=>{ 
        clear_c();
        ctx.globalCompositeOperation="source-over";
        ctx.drawImage(last_img, 0, 0);};
    }
    delete last_img;
}

function save_img() {
    var downloader = document.getElementById('saver');
    var image = document.getElementById('myCanvas').toDataURL("image/png").replace("image/png", "image/octet-stream");
    // replace is not necessary
    downloader.setAttribute("href", image);
}

var xp, yp, w, h;

function upld_img() {
    /*xp = prompt("Enter image's x_position (1-800)");
    if(xp > 800 || xp < 0 || xp == null) {
        alert("x position invalid");
        return;
    }
    yp = prompt("Enter image's y_position (1-800)");
    if(yp > 800 || yp < 0 || yp == null) {
        alert("y position invalid");
        return;
    }
    h = prompt("Enter image's height");
    if(h == null || h < 0) {
        alert("invalid height");
        return;
    }
    w = prompt("Enter image's width");
    if(w == null || w < 0) {
        alert("invalid width");
        return;
    }
    console.log("all input valid");*/
    save_cur('undo');
    //alert(h + '\n' + w);
    var up_btn = document.getElementById('upload');
    up_btn.focus();
    up_btn.click();
}

function load_file(in_f){
    var file = in_f.files[0]; //get input
    const url = URL.createObjectURL(file);
    draw_img(url);
}
function draw_img(imgData){
    var img = new Image();
    img.src = imgData;
    //console.log(img.type);
    img.onload = function(){
        ctx.globalCompositeOperation="source-over";
        //ctx.drawImage(img, xp, yp, w, h);
        ctx.drawImage(img, 0, 0, 600, 400);
    }
}

