# Software Studio 2020 Spring
## Assignment 01 Web Canvas
## 107062335 楊雲丰

### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| color saver                                      | 1~5%      | Y         |


---

### How to use 

    載入畫面後要先點選筆的icon才可以開始畫畫。
    工具區最上面有個橘色外框的色塊會記錄目前筆刷、文字和圖形的顏色。
    點擊顏色區右下角的彩色icon可以叫出調色盤。
    繪製圖形的方式為先在canvas上點一點為參考點，接下來可以隨意在canvas移動，當決定好圖形後再
    點擊一次完成繪製。

### Function description
    1.最上面兩個大的色塊可以用來儲存用過的顏色，當色塊的外框是橘色的時候，每次你換顏色的時候他就
        會跟著換，而外框為白色的則不會變。點擊不是橘色的那塊會使他外框變成橘色，而原本橘色的則會
        變成白色。
    
    2.當滑鼠移動至button上會有一些變化，以及當橡皮擦的大小改變，游標的大小也會變。(共有四段)

### Gitlab page link

    my web page URL: "https://107062335.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    想請問助教為何當我在開啟檔案選擇器(上傳檔案)之前若是先呼叫了prompt，在prompt執行完後有時
    console會出現以下訊息以至於無法開啟檔案選擇器: 
    web_canvas.js:532 File chooser dialog can only be shown with a user activation.
    感謝助教！


